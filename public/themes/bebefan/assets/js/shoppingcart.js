function calculate(el){

    var total = 0;
    setTimeout(function(){
        if(el.hasClass('plus')){
            var adet = parseFloat(el.siblings('.cart-quantity').val())+1
        }else{
            var adet = parseFloat(el.siblings('.cart-quantity').val())-1
        }
        el.siblings('.cart-quantity').val(adet)
        var qty = $('.cart-quantity')
        qty.each(function(){

            var quantity = parseInt($(this).val());
            var price = parseFloat($(this).parents('.quantity').next().find('#price').val())

            total += quantity*price.toFixed(2);
        })

        $('#totalPrice').html(total.toFixed(2))
        $('#lastTotal').html(total.toFixed(2))



    },100)
    return false;
}
$('#toCheckout').click(function(e){
    e.preventDefault();
    var link = $(this).attr('href')
    submitForm(link);

})

function submitForm(link){
    var tr = $('tr')
    var arr = {};
    var i = 0;
    var tot = $(this).find('.tot').val();
    var max = $(this).find('.max').val();

    tr.each(function(){
        var id = $(this).attr('id');

        if(id != undefined && id != null && id != ''){
            var qty = $(this).find('.cart-item-quantity').find('.cart-quantity').val();

            arr[id] = qty;
            i++;
        }

    })


    $.post(link,arr, function(req, res){

      if(req === 'error'){
        window.location.href = '/sepet';
      }else{
        window.location.href = '/checkout';
      }



    })
}
