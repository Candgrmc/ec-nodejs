var options = {
       valueNames: [
           { data: ['product', 'facilities'] }
       ],
       page: 27,
       pagination: true,
       plugins: [
        ListPagination({})
      ]
   };
   var productslist = new List('products', options);

   function stringContainsAll(string, expressions) {
       if(!expressions.length) return true;
       if(!string) return false;

       return expressions.every(function(v) {
           return string.match(v);
       });
   }

   $('.pink-scroll input[type="checkbox"]').on('change', function () {
       updateAll();
   });
   $('#product-type').on('change', function () {
        updateAll();
    });


   function applyFacilities() {
       var searchArray = [];
       $('.pink-scroll input[type="checkbox"]').each(function() {
           if(this.checked) {
               searchArray.push($(this).val());
           }
       });
        productslist.filter(function(item){
           result = stringContainsAll(item.values().product, searchArray);
           return result;
       });
   }

   // function applyCategories(){
   //          productslist.filter(function(item){
   //             search = [$('#product-type').val()];
   //             result = stringContainsAll(item.values().facilities, search);
   //
   //             return (item.visible() && result);
   //         });
   //     }

   function updateAll() {
      productslist.search();
      applyFacilities();
      // applyCategories();
   }
