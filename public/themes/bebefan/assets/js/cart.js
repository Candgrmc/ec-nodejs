function addToCart($this){
    var productOptions = getSelectedOptions();
    var lo = JSON.stringify(productOptions);
    let options = {};
    options = JSON.parse(lo);
    var lop = [];
    $.each(options, function (index, value) {
      lop += index + ' : ' + value + ' ';
    });

    var id = $this.attr('data-id')
    var price = $this.attr('data-price');
    var minimum = $this.attr('data-minimum');
    var img = $this.attr('data-img');
    var name = $this.attr('data-name');
    var slug = $this.attr('data-slug');
    var option = $this.attr('data-option');
    var optioncheck = $this.attr('data-option-check');

    if(optioncheck){
      if(option){
        $.post('/add-to-cart',{id:id,name:name,slug:slug,price:price,minimum:minimum,img:img,option:lop},function(res){
            setTimeout(function(){
              window.location.href = '/';
            },200)

        })
      }else{
        alert('Lütfen beden seçiniz');
      }
    }else{
      var option = '-';
      $.post('/add-to-cart',{id:id,name:name,slug:slug,price:price,minimum:minimum,img:img,option:option},function(res){
          setTimeout(function(){
              window.location.href = '/';
          },200)

      })
    }


}


function getSelectedOptions(){
    var options = {};
    $('.product-opt').each(function(){
        options[$(this).attr('name')] = $(this).val();
    });

    return options;
}
