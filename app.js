var express = require('express');
var path = require('path');
var request = require('request');
var app = express();
var bodyParser = require('body-parser');
var flash = require('connect-flash');
var fs = require('fs');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var flash = require('express-flash');
var toastr = require('express-toastr');
var dotenv = require('dotenv').config();
var chalk = require('chalk');


app.use(cookieParser());
app.use(session({
    secret: 'Pia Ecommerce',
    cookie: { maxAge: 3600000 },
    resave: false,
    saveUninitialized: true
}));
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));
app.use(express.json());       // to support JSON-encoded bodies
app.use(express.urlencoded()); // to support URL-encoded bodies
app.use(flash());
app.use(toastr());
app.set('view engine','ejs');
app.set('views', path.join(__dirname,'views'));
app.use(express.static("public"));
app.use(function (req, res, next) {
    res.locals = {
        siteTitle: "My Website's Title",
        pageTitle: "The Home Page",
        author: "Cory Gross",
        description: "My app's description",
        sesUser : req.session.user,
        sesCart: req.session.cart,
    };
    res.locals.toasts = req.toastr.render();
    next();
});

    /***********\
    # ######### #
    #  ________ #
    #  |Routes| #
    #  -------- #
    # ######### #
  \**----------**/

app.get('/',function(req,res){
    var siteSettings = require('./controllers/globals');

    var product = require('./controllers/product');
    product.products(function(response){
    product.showcase(function(showcase){
    product.discounts(function(discounts){

        siteSettings.settings(function(settings){
            res.render('index',{
                products : JSON.stringify(response.data),
                showcase : JSON.stringify(showcase.data),
                discounts : JSON.stringify(discounts.data),
                best_sellers : JSON.stringify(response.best),
                siteSettings : JSON.stringify(settings),
                siteMenus : siteSettings.menus,
                siteEmails: JSON.stringify(settings.email),
                companyInfo: JSON.stringify(settings.company),
                siteCategories : JSON.stringify(settings.categories),
                brands : JSON.stringify(settings.brands),
            })
        })
    })
    })
    })
});

app.get('/sayfa/:slug',function(req,res){
    var siteSettings = require('./controllers/globals');
    siteSettings.settings(function(settings){

        res.render('pages/page',{
            siteSettings : JSON.stringify(settings),
            siteMenus : siteSettings.menus,
            siteEmails: JSON.stringify(settings.email),
            companyInfo: JSON.stringify(settings.company),
            pages : JSON.stringify(settings.pages),
            slug : req.params.slug,
            siteCategories : JSON.stringify(settings.categories),
        })
    })
})

app.get('/urunler',function(req,res){
    var siteSettings = require('./controllers/globals');
    var product = require('./controllers/product');
    product.products(function(response){
        siteSettings.settings(function(settings){
            res.render('category/products',{
                products : JSON.stringify(response.data),
                siteSettings : JSON.stringify(settings),
                siteMenus : siteSettings.menus,
                siteEmails: JSON.stringify(settings.email),
                companyInfo: JSON.stringify(settings.company),
                siteCategories : JSON.stringify(settings.categories)
            })
        })

    });
})

app.get('/urunler/:type',function(req,res){
        var siteSettings = require('./controllers/globals');
        var product = require('./controllers/product');
        switch (req.params.type){
            case 'cok-satanlar':
                product.products(function(response){
                    siteSettings.settings(function(settings){
                        res.render('category/products',{
                            products : JSON.stringify(response.best),
                            siteSettings : JSON.stringify(settings),
                            siteMenus : siteSettings.menus,
                            siteEmails: JSON.stringify(settings.email),
                            companyInfo: JSON.stringify(settings.company),
                            siteCategories : JSON.stringify(settings.categories)
                        })
                    })

                });
                break;
            case 'indirimli-urunler':
                product.onSaleProducts(function(response){
                    siteSettings.settings(function(settings){
                        res.render('category/products',{
                            products : JSON.stringify(response.data),
                            siteSettings : JSON.stringify(settings),
                            siteMenus : siteSettings.menus,
                            siteEmails: JSON.stringify(settings.email),
                            companyInfo: JSON.stringify(settings.company),
                            siteCategories : JSON.stringify(settings.categories)
                        })
                    })
                });
                break;

        }
});

app.get('/urun/:slug',function(req,res){
    var product       = require('./controllers/product');
    var siteSettings  = require('./controllers/globals');

    product.productDetail(req.params.slug,function(response){

        // var productsAlike = JSON.stringify(response.productsAlike);
        siteSettings.settings(function(settings){
            res.render('product/detail',{
                product : JSON.stringify(response),
                siteSettings : JSON.stringify(settings),
                siteMenus : siteSettings.menus,
                siteEmails: JSON.stringify(settings.email),
                companyInfo: JSON.stringify(settings.company),
                siteCategories : JSON.stringify(settings.categories),
            })
        })
        });
});

app.get('/kategori/:category',function(req,res){
    if(req.params.category){
        var category      = require('./controllers/category');
        var siteSettings  = require('./controllers/globals');

        category.getCategoryProducts(req.params.category,function(response){
            siteSettings.settings(function(settings){
                res.render('category/products',{
                    products : JSON.stringify(response),
                    siteSettings : JSON.stringify(settings),
                    siteMenus : siteSettings.menus,
                    siteEmails: JSON.stringify(settings.email),
                    companyInfo: JSON.stringify(settings.company),
                    siteCategories : JSON.stringify(settings.categories),
                })
            })

        });
    }

});

app.post('/register',function(req,res){
    var user = require('./controllers/user');
    user.register(req.body,function(response){
        if(response.status == 'success'){
          req.toastr.success(response.response);
          res.redirect('/');
        }else{
          req.toastr.error(response.response);
          res.redirect('/');
        }
    })
});

app.post('/login',function(req,res,next){
    if(req.session.user){

        res.redirect('/');
        next

    }else{
        var login = require('./controllers/auth/login');
        login.log_me_in(req.body,function(response){
            var status = JSON.parse(response).status;
            if(status == 'success'){
                var customer = JSON.parse(response).customer;
                req.session.user = customer;
                req.toastr.success('Giriş işleminiz başarılı.');
                res.redirect('/');
                next
            }else{
              req.toastr.error('Giriş işleminiz başarısız!');
              res.redirect('/');
            }
        })
    }
});

app.get('/login/facebook', function(req,res,next){
  if(req.session.user){
    res.redricet('/');
    next
  }else{
    var login = require('./controllers/auth/social');
    login.social(req.body, function(response){
      var status = JSON.parse(response);

    })
  }
});

app.get('/login/facebook/callback', function(req,res,next){
  if(req.session.user){
    res.redirect('/');
    next
  }else{

  }
});

app.get('/logout',function(req,res){
    req.session.user = null;
    req.toastr.info('Çıkış işlemi yapıldı.');
    res.redirect('/');
});
/*
    Shopping Cart
 */
app.post('/add-to-cart',function(req,res){
    if(req.session.cart){
        var Cart = req.session.cart;

        Cart.forEach(function(item){
          if(item.id == req.body.id && item.option == req.body.option){
                item.quantity = item.quantity+1 ;
                req.session.cart = Cart;
                req.toastr.info('Sepet Güncellendi.');
                res.end('updated');
            }else{
                var newProduct = req.body;
                newProduct.quantity = 1;
                Cart.push(newProduct);
                req.session.cart = Cart;
                req.toastr.success('Ürün başarıyla sepete eklendi.');
                res.end('updated');
            }
        })
    }else{
        var Cart = [];
        var Product = req.body;
        Product.quantity = 1;
        Cart.push(Product);

        req.session.cart = Cart;
        req.toastr.success('Ürün başarıyla sepete eklendi.');
        res.end('created');
    }
});

app.get('/remove-from-cart/:id',function(req,res){
    var Cart = req.session.cart;
    Cart.forEach(function(cart){
        var cartid = cart.id;
        var bodyid = req.params.id;
        if(cartid == bodyid){
            Cart.splice(cart, 1)
        }
        console.log(cart);
    })
    if(Cart.length){
        req.session.cart = Cart;
    }else{
        req.session.cart = null;
    }

    backURL=req.header('Referer') || '/';

    res.redirect(backURL);
})

app.get('/sepet',function(req,res){
    var siteSettings = require('./controllers/globals');
    var categories = siteSettings.categories;
    var cart = req.session.cart;
    if(cart == null){
      req.toastr.info('sepetnizde ürün mevcut değil.');
      return res.redirect('/');
    }
    siteSettings.settings(function(settings){
        res.render('cart/shopping-cart',{
            siteSettings : JSON.stringify(settings),
            siteMenus : siteSettings.menus,
            siteEmails: JSON.stringify(settings.email),
            companyInfo: JSON.stringify(settings.company),
            siteCategories : JSON.stringify(settings.categories),
        })
    })
})

app.post('/checkout_update',function(req,res){
    var Cart = req.session.cart;
    var total = 0;
    var max   = 0;
    Cart.forEach(function(cart){
      var limit = cart.minimum;
      max += Math.max(limit);
      total += cart.price * cart.quantity;
        var id = cart.id;
        if(req.body[id]){
            cart.quantity = req.body[id];
        }
    })
    var tot = total.toFixed(2);
    if(tot < max){
      req.toastr.warning('ÖZEL İNDİRİMLİ ürünleri satın alabilmek için sepet tutarınız '+max+' TL olmalıdır.');
      res.send('error');

    }else{
      req.session.cart = Cart;
      res.send('success');
    }
})

app.get('/checkout',function(req,res){

    var siteSettings = require('./controllers/globals');
    var categories = siteSettings.categories;
    var cart = req.session.cart;
    if(cart == null){
      req.toastr.info('sepetnizde ürün mevcut değil.');
      return res.redirect('/');
    }
    siteSettings.settings(function(settings){
        res.render('cart/checkout',{
            siteSettings : JSON.stringify(settings),
            siteMenus : siteSettings.menus,
            siteEmails: JSON.stringify(settings.email),
            companyInfo: JSON.stringify(settings.company),
            siteCategories : JSON.stringify(settings.categories),
            county : JSON.stringify(settings.county),
        })
    })

})

app.get('/success',function(req,res){

    var siteSettings = require('./controllers/globals');
    var categories = siteSettings.categories;
    siteSettings.settings(function(settings){
        res.render('cart/success',{
            siteSettings : JSON.stringify(settings),
            siteMenus : siteSettings.menus,
            siteEmails: JSON.stringify(settings.email),
            companyInfo: JSON.stringify(settings.company),
            siteCategories : JSON.stringify(settings.categories),
            county : JSON.stringify(settings.county),
        })
    })

})

app.post('/complete_order',function(req,res){
    var cart = req.session.cart;
    var shop = require('./controllers/Cart');
    var orderInfo = req.body
    if(req.session.user){
        orderInfo['user'] = req.session.user;
    }

    shop.completeOrder(req.session.cart,orderInfo,function(response){
        status    = JSON.parse(response).status;
        response  = JSON.parse(response).response;
        if(status == 'success'){
          req.toastr.success(response);
          delete req.session.cart;
          res.redirect('success');
        }else{
          req.toastr.error(response);
          res.redirect('/');
        }
    })
})
/*
    Profil Routes
 */
app.get('/profil',function(req,res){
    var siteSettings = require('./controllers/globals');

    siteSettings.settings(function(settings){
        if(req.session.user){
            res.render('profile/profile',{
                siteSettings : JSON.stringify(settings),
                siteMenus : siteSettings.menus,
                siteEmails: JSON.stringify(settings.email),
                companyInfo: JSON.stringify(settings.company),
                siteCategories : JSON.stringify(settings.categories),
            })
        }else{
            res.render('error/auth',{
                siteSettings : JSON.stringify(settings),
                siteMenus : siteSettings.menus,
                siteEmails: JSON.stringify(settings.email),
                companyInfo: JSON.stringify(settings.company),
                siteCategories : JSON.stringify(settings.categories),
            })
        }
    })
});

app.get('/profil/:page',function(req,res){
    var user = require('./controllers/user');
    var siteSettings = require('./controllers/globals');
    if(req.params.page){
        switch(req.params.page){
            case 'account':
                siteSettings.settings(function(settings){
                    res.render('profile/inc/account',{
                        siteSettings : JSON.stringify(settings),
                        siteMenus : siteSettings.menus,
                        siteEmails: JSON.stringify(settings.email),
                        companyInfo: JSON.stringify(settings.company),
                        siteCategories : JSON.stringify(settings.categories),
                    })
                })
                break;
            case 'password':

                siteSettings.settings(function(settings){
                    res.render('profile/inc/password',{
                        siteSettings : JSON.stringify(settings),
                        siteMenus : siteSettings.menus,
                        siteEmails: JSON.stringify(settings.email),
                        companyInfo: JSON.stringify(settings.company),
                        siteCategories : JSON.stringify(settings.categories),
                    })
                })
                break;
            case 'address':
                user.userInfo(req.session.user.customer_id,function(customer){
                    siteSettings.settings(function(settings){
                        res.render('profile/inc/address',{
                            siteSettings : JSON.stringify(settings),
                            siteMenus : siteSettings.menus,
                            siteEmails: JSON.stringify(settings.email),
                            companyInfo: JSON.stringify(settings.company),
                            siteCategories : JSON.stringify(settings.categories),
                            customer : customer
                        })
                    })
                });

                break;
            case 'orders':
            user.userInfo(req.session.user.customer_id,function(customer){
                siteSettings.settings(function(settings){
                    res.render('profile/inc/orders',{
                        siteSettings : JSON.stringify(settings),
                        siteMenus : siteSettings.menus,
                        siteEmails: JSON.stringify(settings.email),
                        companyInfo: JSON.stringify(settings.company),
                        siteCategories : JSON.stringify(settings.categories),
                        customer : customer
                    })
                })
              });
                break;
            case 'invoices':
                siteSettings.settings(function(settings){
                    res.render('profile/inc/invoices',{
                        siteSettings : JSON.stringify(settings),
                        siteMenus : siteSettings.menus,
                        siteEmails: JSON.stringify(settings.email),
                        companyInfo: JSON.stringify(settings.company),
                        siteCategories : JSON.stringify(settings.categories),
                    })
                })
                break;
        }
    }else{

    }
})

app.post('/update_address',function(req,res){
    var address = require('./controllers/address');
    address.updateAddress(req.session.user,req.body,function(response){
        if(response == 'success'){
            req.toastr.success('Adresiniz başarıyla güncellendi.');
            backURL=req.header('Referer') || '/';
            res.redirect(backURL);
        }
    })
});

app.get('/delete_address/:id',function(req,res){
    var address = require('./controllers/address');
    address.deleteAddress(req.session.user,req.params.id,function(result){
        if(result == 'success'){
            req.toastr.info('Adresiniz silindi.');
            backURL=req.header('Referer') || '/';
            res.redirect(backURL);
        }
    })
});

app.get('/set_primary_address/:id',function(req,res){
    var address = require('./controllers/address');
    address.setPrimary(req.session.user,req.params.id,function(result){
        if(result == 'success'){
            req.toastr.info('Birincil adres seçildi.');
            backURL=req.header('Referer') || '/';
            res.redirect(backURL);
        }
    })
});

app.post('/update_user',function(req,res){
    var user = require('./controllers/user');
    user.updateUser(req.session.user,req.body,function(response){
        response = JSON.parse(response);
        if(response.status == 'success'){
            req.toastr.success('Bilgileriniz başarıyla güncellendi.');
            backURL=req.header('Referer') || '/';
            req.session.user = response.customer;
            res.redirect(backURL);

        }
    })
})

app.post('/update_password',function(req,res){
    var user = require('./controllers/user');
    var password = req.body.password;
    var password_confirm = req.body.password_confirm;

    if(password == password_confirm){
        user.updatePassword(req.session.user,password,function(response){
            if(response == 'success'){
                req.toastr.success('Şifreniz başarıyla güncellendi.');
                backURL=req.header('Referer') || '/';
                res.redirect(backURL);
            }else{
                req.toastr.error('Şifreniz güncellenemedi.');
                res.send('Hata');
            }
        })
    }else{
        res.send('Şifreler Tutmuyor');
    }

})

app.post('/arama',function(req,res){
    var s = require('./controllers/search');
    var siteSettings = require('./controllers/globals');
    s.search(req.body,function(products){
        response = JSON.stringify(products);
        siteSettings.settings(function(settings){
            res.render('category/search',{
                products : response,
                siteSettings : JSON.stringify(settings),
                siteMenus : siteSettings.menus,
                siteEmails: JSON.stringify(settings.email),
                companyInfo: JSON.stringify(settings.company),
                siteCategories : JSON.stringify(settings.categories),
            })
        })

    })
});

app.get('/clear-cache',function(req,res){
    delete require.cache[require.resolve('./controllers/globals')];
    res.send('1');
});

app.get('*', function(req, res){
    var siteSettings = require('./controllers/globals');
    siteSettings.settings(function(settings){
        res.render('error/404',{
            siteSettings : JSON.stringify(settings),
            siteMenus : siteSettings.menus,
            siteEmails: JSON.stringify(settings.email),
            companyInfo: JSON.stringify(settings.company),
            siteCategories : JSON.stringify(settings.categories),
        })
    })
});

var server = app.listen(process.env.PORT, function(){
  console.log(chalk.red('Working on 8082 port'))

});
